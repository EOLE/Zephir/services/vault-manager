def db_exists(cursor, name):
    cursor.execute("""SELECT 1 from pg_database WHERE datname='{}';""".format(name))
    return cursor.fetchone() is not None


def create_database(cursor, name):
    cursor.execute("""CREATE DATABASE "{}";""".format(name))


def create_role_admin(cursor, name):
    cursor.execute('CREATE ROLE "{0}_admin"'.format(name))
    cursor.execute('GRANT ALL PRIVILEGES ON DATABASE "{0}" TO "{0}_admin"'.format(name))
    cursor.execute('GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO "{0}_admin"'.format(name))
    cursor.execute('GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO "{0}_admin"'.format(name))


def create_persistent_role(cursor, name, password):
    cursor.execute("""CREATE ROLE "{0}_persistent" WITH LOGIN PASSWORD '{1}' IN ROLE "{0}_admin";""".format(name, password))
