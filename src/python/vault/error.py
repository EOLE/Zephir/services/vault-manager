class VaultError(Exception):
    """Base class of :class:`Vault` exceptions
    """


class VaultErrorSecretNotAvailable(VaultError):
    """No available secret
    """
