import aiohttp
from os import urandom
from binascii import hexlify
from json import dumps, loads
import requests
import json

from .query import db_exists, create_database, create_role_admin, create_persistent_role


class Vault:
    VAULTHOST = "localhost:8200"

    def db_exists(self, cursor, name):
        return db_exists(cursor, name)

    def create_database(self, cursor, name, user, password):
        """create database + postgres role + vault configuration
        """
        #create database/config/xxx
        data = dumps({
                "plugin_name": "postgresql-database-plugin",
                "allowed_roles": "{0}_admin".format(name),
                "connection_url": "postgresql://{}:{}@postgres:5432/postgres?sslmode=disable".format(user, password)
        })
        headers = {"X-Vault-Token": self.get_token()}
        response = requests.post("http://{}/v1/database/config/{}".format(self.VAULTHOST, name), data=data, headers=headers)
        if response.status_code != 200:
            raise Exception('cannot add database "{}" configuration to vault: {}, {}'.format(name, response.reason, response.text))

        #create database/roles/xxx_admin associated with previous database/config/xxx
        create_database(cursor, name)
        create_role_admin(cursor, name)
        data = dumps({
                "db_name": name,
                "creation_statements": """CREATE ROLE "{{{{name}}}}" WITH LOGIN PASSWORD '{{{{password}}}}' VALID UNTIL '{{{{expiration}}}}' IN ROLE "{0}_admin";""".format(name),
                "default_ttl": "2h",
                "revocation_statements": """SELECT pg_terminate_backend(pid) FROM pg_stat_activity WHERE usename = '{{name}}'; DROP ROLE "{{name}}";""",
                "max_ttl": "24h"
        })
        response = requests.post("http://{}/v1/database/roles/{}_admin".format(self.VAULTHOST, name), data=data, headers=headers)
        if response.status_code != 204:
            raise Exception('cannot add role "{}" configuration to vault: {}, {}'.format(name, response.reason, response.text))

    def get_token(self):
        with open('/srv/vault_secrets/root_token') as token_file:
            return token_file.read().strip()

    def create_temporary_user(self, name):
        #launch database/creds/xxx_admin
        headers = {"X-Vault-Token": self.get_token()}
        response = requests.get("http://{}/v1/database/creds/{}_admin".format(self.VAULTHOST, name), headers=headers)
        if response.status_code != 200:
            raise Exception('cannot apply role "{}" configuration to vault: {}, {}'.format(name, response.reason, response.text))
        try:
            response_data = loads(response.text)['data']
            username = response_data['username']
            password = response_data['password']
        except Exception as err:
            raise Exception('cannot retrieved information from vault: {}'.format(err))
        else:
            return {'username': username, 'password': password}

    async def create_persistent_user(self, cursor, name):
        username = '{}_persistent'.format(name)
        secret = await self.get_secret(username)
        if 'data' in secret:
            password = secret['data']['password']
        else:
            password = hexlify(urandom(24)).decode()
            create_persistent_role(cursor, name, password)
            await self.set_secret(username, {'password': password})
        return {'username': username,
                'password': password}



    async def set_secret(self, secretkey, secret):
        root_token = self.get_token()
        vault_url = "http://{}/v1/secret/{}".format(self.VAULTHOST, secretkey)
        headers = {"X-Vault-Token": root_token}

        async with aiohttp.ClientSession() as session:
            async with session.post(vault_url, data=json.dumps(secret), headers=headers) as response:
                return await response.text()

    async def get_secret(self, secretkey):
        root_token = self.get_token()
        vault_url = "http://{}/v1/secret/{}".format(self.VAULTHOST, secretkey)
        headers = {"X-Vault-Token": root_token}

        async with aiohttp.ClientSession() as session:
            async with session.get(vault_url, headers=headers) as response:
                resp = await response.json()
        return resp
